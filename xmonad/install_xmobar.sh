# C dependencies
sudo apt install c2hs libiw-dev libXpm

# Haskell dependencies
cabal install X11-xft alsa-mixer

# Install xmobar with all extensions allowed
cabal install xmobar --flags="all_extensions"
