#!/bin/bash

printf "\nInstalling Haskell Platform\n"
sudo apt install -y haskell-platform

printf "Installing X11 libraries"
printf "Dependencies for xmonad"
sudo apt install -y libx11-dev libxinerama-dev libxext-dev libxrandr-dev libxss-dev

printf "Dependencies for xmonad-contrib"
sudo apt install -y libxft-dev

printf "Update list of available haskell packages"
cabal update

printf "Installing xmonad"
cabal install xmonad xmonad-contrib

printf "Setting up default terminal"
