#!/bin/bash

# Install zsh
printf "\n\nInstalling zsh, if it's not installed\n"
sudo apt install -y zsh
printf "Zsh installed\n"

# Install oh-my-zsh
printf "Installing oh-my-zsh\n"
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Set zsh as default shell
printf "Setting zsh as the default shell\n"
chsh -s $(which zsh)

# Copy config files
cp $(pwd)/.z* ~/

# Reload .zshrc
source $(pwd)/.zshrc

printf "Log out and back in to see your new shell"
