#!/bin/bash
echo "Test initial setup"

# Check for updates?
read -p "Check for updates? (Y/n)" update
if [ $update == 'Y' ]
then
	sudo apt update
fi

# Install lightdm?
read -p "Install lightdm? (Y/n)" lightdm

# Install urxvt?
read -p "Install urxvt? (Y/n)" urxvt

# Install kitty?
read -p "Install kitty? (Y/n)" kitty

# Install and configure zsh?
read -p "Install zsh? (Y/n)" zsh

# Install vim?
read -p "Install vim? (Y/n)" vim

# Install node?
read -p "Install node? (Y/n)" node

# Install Xmonad?
# TODO: ask to choose one of a number of environments
read -p "Install xmonad? (Y/n)" xmonad

# Install compton?
read -p "Install compton? (Y/n)" compton

# Install polybar?
read -p "Install polybar? (Y/n)" polybar

# Install firefox from default apt repo?
read -p "Install firefox? (Y/n)" firefox

# Install chromium?
read -p "Install chromium? (Y/n)" chromium

# Install spotify?
read -p "Install spotify? (Y/n)" spotify

# Install Docker?
read -p "Install Docker? (Y/n)" docker

##################################################
##             Installation stage               ##
##################################################

# Install essentials
source essentials/install_essentials.sh

# Batch all sudo apt install into one command
if [ $lightdm == 'Y' ]
then
    sudo apt install -y lightdm xserver-xephyr
    # configure lightdm to start DE/WM

    # Setup lightdm as the default display manager
    sudo dpkg-reconfigure lightdm

    # Copy config files
    cp $(pwd)/dotfiles/ $HOME
fi

# Install and configure urxvt (rxvt-unicode)
if [ $urxvt == 'Y' ]
then
    sudo apt install -y rxvt-unicode
fi

# Install kitty
if [ $kitty == 'Y' ]
then
    sudo apt install -y kitty
fi

# Install and configure zsh
if [ $zsh == 'Y' ]
then
    source zsh/install_zsh.sh
fi

# Install vim
if [ $vim == 'Y' ]
then
    source vim/install_vim.sh
fi

# Install node
if [ $node == 'Y' ]
then
    sudo curl -sL https://deb.nodesource.com/setup_12.x | bash -
    sudo apt install -y nodejs
fi

# Install xmonad
if [ $xmonad == 'Y' ]
then
    # read -p "Install xmobar? (Y/n)" xmobar
    source xmonad/install_xmonad.sh
fi

# Install compton
if [ $compton == 'Y' ]
then
    source compton/install_compton.sh
fi

# Install polybar
if [ $polybar == 'Y' ]
then
    source polybar/install_polybar.sh
fi

# Install firefox
if [ $firefox == 'Y' ]
then
    sudo apt install -y firefox
    # Setup as default in xmonad.hs or whatever other config file
fi

# Install chromium
if [ $chromium == 'Y' ]
then
    sudo apt install -y chromium
fi

# TODO: Install spotify
if [ $spotify == 'Y' ]
then
    # I think there's a snap
    # 1. Add the Spotify repository signing keys to be able to verify downloaded packages
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90

    # 2. Add the Spotify repository
    echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list

    # 3. Update list of available packages
    sudo apt-get update

    # 4. Install Spotify
    sudo apt-get install spotify-client
fi

# TODO: Install docker
if [ $docker == 'Y' ]
then
    # Use get.docker script
    source docker/install_docker.sh
fi

# Create directories


# Install sound
# Check if this is enough
# pavucontrol includes pulseaudio
# sudo apt install -y pavucontrol
