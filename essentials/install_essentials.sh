#!/bin/bash

# Install essentials
printf "\nInstalling essentials\n"
sudo apt install -y curl tmux man-db build-essential git tree firmware-iwlwifi iw wireless-tools network-manager net-tools snapd feh xclip timeshift
printf "Essentials installed\n"

# Optional
sudo apt install -y compton suckless-tools font-manager

# Install audio
sudo apt install -y  alsa-utils pavucontrol

# Install bluetooth packages
sudo apt install -y bluetooth bluez bluez-tools rfkill blueman

# TODO: install fonts

# Install suckless-tools?

# Install x11-xserver-utils (for xrdb, which is used to reload .Xresources)
sudo apt install -y x11-xserver-utils
