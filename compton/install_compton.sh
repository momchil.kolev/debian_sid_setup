#!/bin/bash

# Install compton
sudo apt install -y compton

# Copy over config files
cp ./compton.conf ./compton.conf.default ~/.config/compton/
