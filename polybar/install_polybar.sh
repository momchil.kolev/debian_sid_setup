#!/bin/bash

# Install dependencies
sudo apt install -y clang cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev libxcb-xrm-dev python2 xcb-proto libasound2-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libpulse-dev  gcc git libxcb-composite0-dev python-xcbgen
# Install additional dependencies
sudo apt install -y cairo-dock

# Install polybar
# Create Downloads if it doesn't exist
if [ -d "~/Downloads" ];
then
    echo "Exists";
else
    echo "Doesn't exist";
fi

# Go to ~/Downloads
cd ~/Downloads

# Download tar
curl -sLO https://github.com/jaagr/polybar/releases/download/3.3.1/polybar-3.3.1.tar

# Extract it
tar xvf polybar-*.tar

# Go into extracted folder
cd polybar

# Build polybar
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install

# Copy config over
cp ./config ~/.config/polybar/
