[bar/mybar]
override-redirect = true
bottom = false
monitor = ${env:MONITOR:}

width = 100%
height = 30
padding = 2 
font-0 = Hack-Regular:size=10

background = #80000000
tray-background = #ff0000

module-margin = 2 
modules-left = cpu memory
modules-right = network pulseaudio date 

[module/cpu]
type = internal/cpu

format = <label> <ramp-coreload>

label = CPU %percentage%%

; Spacing between individual per-core ramps
ramp-coreload-spacing = 1
ramp-coreload-0 = ▁
ramp-coreload-1 = ▂
ramp-coreload-2 = ▃
ramp-coreload-3 = ▄
ramp-coreload-4 = ▅
ramp-coreload-5 = ▆
ramp-coreload-6 = ▇
ramp-coreload-7 = █

[module/memory]
type = internal/memory

interval = 5

format = <label> <bar-used>

label = RAM %gb_used%/%gb_free%

; Only applies if <bar-used> is used
bar-used-indicator =
bar-used-width = 50
bar-used-foreground-0 = #55aa55
bar-used-foreground-1 = #557755
bar-used-foreground-2 = #f5a70a
bar-used-foreground-3 = #ff5555
bar-used-fill = ▐
bar-used-empty = ▐
bar-used-empty-foreground = #444444

; Only applies if <ramp-used> is used
ramp-used-0 = ▁
ramp-used-1 = ▂
ramp-used-2 = ▃
ramp-used-3 = ▄
ramp-used-4 = ▅
ramp-used-5 = ▆
ramp-used-6 = ▇
ramp-used-7 = █

; Only applies if <ramp-free> is used
ramp-free-0 = ▁
ramp-free-1 = ▂
ramp-free-2 = ▃
ramp-free-3 = ▄
ramp-free-4 = ▅
ramp-free-5 = ▆
ramp-free-6 = ▇
ramp-free-7 = █

[module/network]
type = internal/network
interface = enp59s0

; Seconds to sleep between updates
; Default: 1
interval = 5.0

; Test connectivity every Nth update
; A value of 0 disables the feature
; NOTE: Experimental (needs more testing)
; Default: 0
;ping-interval = 3

; @deprecated: Define min width using token specifiers (%downspeed:min% and %upspeed:min%)
; Minimum output width of upload/download rate
; Default: 3
upspeed-minwidth = 5

; Accumulate values from all interfaces
; when querying for up/downspeed rate
; Default: false
accumulate-stats = true

; Consider an `UNKNOWN` interface state as up.
; Some devices have an unknown state, even when they're running
; Default: false
unknown-as-up = true

; Available tags:
;   <label-connected> (default)
;   <ramp-signal>
format-connected = <ramp-signal> <label-connected>

; Available tags:
;   <label-disconnected> (default)
format-disconnected = <label-disconnected>

; Available tags:
;   <label-connected> (default)
;   <label-packetloss>
;   <animation-packetloss>
format-packetloss = <animation-packetloss> <label-connected>

; All labels support the following tokens:
;   %ifname%    [wireless+wired]
;   %local_ip%  [wireless+wired]
;   %local_ip6% [wireless+wired]
;   %essid%     [wireless]
;   %signal%    [wireless]
;   %upspeed%   [wireless+wired]
;   %downspeed% [wireless+wired]
;   %linkspeed% [wired]

; Default: %ifname% %local_ip%
label-connected = %signal% %upspeed% %downspeed:9%
label-connected-foreground = #eefafafa

; Default: (none)
label-disconnected = not connected
label-disconnected-foreground = #66ffffff

; Default: (none)
;label-packetloss = %essid%
;label-packetloss-foreground = #eefafafa

; Only applies if <ramp-signal> is used
ramp-signal-0 = 😱
ramp-signal-1 = 😠
ramp-signal-2 = 😒
ramp-signal-3 = 😊
ramp-signal-4 = 😃
ramp-signal-5 = 😈

; Only applies if <animation-packetloss> is used
animation-packetloss-0 = ⚠
animation-packetloss-0-foreground = #ffa64c
animation-packetloss-1 = 📶
animation-packetloss-1-foreground = #000000
; Framerate in milliseconds
animation-packetloss-framerate = 500

[module/pulseaudio]
type = internal/pulseaudio
sink = alsa_output.pci-0000_00_1f.3.analog-stereo
use-ui-max = true
interval = 5

format-volume = <ramp-volume> <label-volume>
label-muted = 🔇 muted
label-muted-foreground = #666
ramp-volume-0 = 🔈
ramp-volume-1 = 🔉
ramp-volume-2 = 🔊

[module/date]
type = internal/date
interval = 1.0
date = %Y-%m-%d
time = %H:%M

format = 🕓 <label>
; format-background = #000000
; format-foreground = #fffV

label = %date% %time%
label-font = 3
label-foreground = #fff
